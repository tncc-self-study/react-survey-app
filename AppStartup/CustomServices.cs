using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Configuration;
using react_survey_app.Models;
using react_survey_app.Data.Infrastructure;
using MongoDbGenericRepository;

namespace react_survey_app.AppStartup
{
    public static class CustomServices
    {
        #region Constants

        static string ConnString = "MongoConnection:ConnectionString";
        static string DbName = "MongoConnection:DatabaseName";

        #endregion

        #region Properties

        static Settings DbConnSettings { get; set; }

        #endregion

        public static void AddCustomServices(this IServiceCollection services, IConfiguration config) {
            BindConnStrConfigurations(config);

            var ctx = GetMongoDbContext();
            services.AddTransient<IRepository>(provider => new Repository(ctx));
            services.AddCustomCors();
        }

      static void AddCustomCors(this IServiceCollection services) {
        services.AddCors(options =>
        {
            options.AddPolicy("AllowAllOrigins",
            builder =>
            {
                builder.AllowAnyMethod().AllowAnyHeader().AllowAnyOrigin();
            });
        });
      }

        static void BindConnStrConfigurations(IConfiguration configuration) {
            var conn = configuration.GetSection(ConnString).Value;
            var dbName = configuration.GetSection(DbName).Value;
            DbConnSettings = new Settings { ConnectionString = conn, DatabaseName = dbName };
        }

        static IMongoDbContext GetMongoDbContext() {
            return new MongoDbContext(DbConnSettings.ConnectionString, DbConnSettings.DatabaseName);
        }
    }
}
