using System;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using react_survey_app.Auth;
using react_survey_app.Models;
using static react_survey_app.Auth.TokensHelper.Constants.Strings;

namespace react_survey_app.AppStartup
{
    public static class JwtServices
    {
        public const string ApiPolicyKey = "ApiUser";
        private const string Phrase = "eendagop'nreendagwasdaargoudonderdiereenbooggewees"; // Get from super secret source

        public static void AddJwtAuth(this IServiceCollection services, IConfiguration config) {
            services.AddOptions();
            services.AddSingleton<IJwtFactory, JwtFactory>();

            var signingKey = GetSymmetricSecurityKey();
            var jwtAppSettingOptions = config.GetSection(nameof(JwtIssuerOptions));
            services.Configure<JwtIssuerOptions>(options =>
            {
              options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
              options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
              options.SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);
            });

            var tokenValidationParameters = GetTokenValidationParameters(jwtAppSettingOptions);
            services.AddAuthentication(options =>
            {
              options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
              options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(configureOptions =>
            {
              configureOptions.ClaimsIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
              configureOptions.TokenValidationParameters = tokenValidationParameters;
              configureOptions.SaveToken = true;
            });

            services.AddAuthorization(options =>
            {
              options.AddPolicy(ApiPolicyKey, policy => policy.RequireClaim(JwtClaimIdentifiers.Rol, JwtClaims.ApiAccess));
            });
        }

        static SymmetricSecurityKey GetSymmetricSecurityKey() {
          return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Phrase));
        }

        static TokenValidationParameters GetTokenValidationParameters(IConfigurationSection options) {
          var signingKey = GetSymmetricSecurityKey();
          return new TokenValidationParameters
          {
            ValidateIssuer = true,
            ValidIssuer = options[nameof(JwtIssuerOptions.Issuer)],

            ValidateAudience = true,
            ValidAudience = options[nameof(JwtIssuerOptions.Audience)],

            ValidateIssuerSigningKey = true,
            IssuerSigningKey = signingKey,

            RequireExpirationTime = false,
            ValidateLifetime = true,
            ClockSkew = TimeSpan.Zero
          };
        }
    }
}
