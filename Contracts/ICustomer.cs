namespace react_survey_app.Contracts
{
    public interface ICustomer
    {
        string Email { get; set; }
        string Mobile { get; set; }
        string Fax { get; set; }
        string Telephone { get; set; }
        string Town { get; set; }
        string Area { get; set; }
        bool IsCurrentCustomer { get; set; }
        string CustomerName { get; set; }
    }
}
