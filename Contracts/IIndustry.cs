namespace react_survey_app.Contracts
{
    public interface IIndustry
    {
        bool Transporter { get; set; }
        bool Distributor { get; set; }
        bool Manufacturer { get; set; }
        bool Reseller { get; set; }
        bool Farmer { get; set; }
        bool Earthmoving { get; set; }
        bool Heavyhaulage { get; set; }
        bool Crossborder { get; set; }
        bool OtherIndustry { get; set; }
    }
}
