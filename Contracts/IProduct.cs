namespace react_survey_app.Contracts
{
    public interface IProduct
    {
        bool Mercedes { get; set; }
        bool FreightLiner { get; set; }
        bool Iveco { get; set; }
        bool Truckbodyparts { get; set; }
        bool Man { get; set; }
        bool Volvo { get; set; }
        bool International { get; set; }
        bool Tankers { get; set; }
        bool Jonesco { get; set; }
        bool Truckaccessories { get; set; }
        bool Scania { get; set; }
        bool Renault { get; set; }
        bool Trailers { get; set; }
        bool Fueldefend { get; set; }
        bool Daf { get; set; }
        bool OtherProduct { get; set; }
    }
}
