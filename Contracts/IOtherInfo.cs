namespace react_survey_app.Contracts
{
    public interface IOtherInfo
    {
        string Comments { get; set; }
        bool IsUrgent { get; set; }
        string Rep { get; set; }
    }
}
