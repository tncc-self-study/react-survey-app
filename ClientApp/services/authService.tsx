import wretch from 'wretch';

const apiUrlConfig: any = require('serverApiUrl');
const tokenUrl: string = 'survey/login';
const tokenKey: string = 'token';

const AuthService = {
  setAuthToken(token: string) {
    localStorage.setItem(tokenKey, token);
  },

  getSavedToken() {
    return localStorage.getItem(tokenKey);
  },

  isAuthenticated() {
    return localStorage.getItem(tokenKey) != null;
  },

  login(username: string, password: string) {
    const actionUrl: string = apiUrlConfig.serverUrl + tokenUrl;
    const body = JSON.stringify({ username: username, password: password });

    return wretch(actionUrl)
           .content('application/json')
           .accept('application/json')
           .post(body)
           .json();
  },

  logOut() {
    localStorage.removeItem(tokenKey);
  },

  /** Login with a callback and callback Args (internally test user and password is set) */
  loginWithCallback(callback: any, callbackArgs: any) {
    const dummyUser = 'I_AM_ROOT'; // !!!! NOT FOR PRODUCTION USE - RECEIVE THIS FROM CLIENT INPUT
    const dummyPass = 'I_AM_ROOT'; // !!!! NOT FOR PRODUCTION USE - RECEIVE THIS FROM CLIENT INPUT
    AuthService.login(dummyUser, dummyPass)
                .then(function(response) {
                    AuthService.setAuthToken(response.auth_token);
                    callback(callbackArgs);
                });
  }
};

export default AuthService;
