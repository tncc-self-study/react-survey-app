import * as React from 'react';
import { Guid } from "guid-typescript";
import { ICheckboxItem } from "../contracts/ICheckboxItem";

const cssHelper = {
    getLabelCss(index: number) {
        return index === 0 ? "checkbox-inline col-sm-3 checkbox-label" : "checkbox-inline col-sm-3";
    }
}

const UtilityService = {
    createCheckboxItem(chkboxItem: ICheckboxItem): { id: string, key: string, label: string, className: string, groupIndex: number } {
        return { id: chkboxItem.id, key: chkboxItem.key, label: chkboxItem.label, className: chkboxItem.className, groupIndex: chkboxItem.groupIndex };
    },
    createSchemaCheckboxItem(fieldName: string, type: string, title: string, defaultValue: boolean) {
        return { fieldName: '' + fieldName + '', type: '' + type + '' , title: '' + title + '' };
    },
    createBootstrapCheckboxItem(item: ICheckboxItem, index: number, inputHandler: any) {
        const css: string = cssHelper.getLabelCss(index);
        return( 
            <label key={item.id} className={css}>
                <input type="checkbox" name={item.key} value={item.key} onChange={inputHandler} />{item.label}
            </label>
               );
    },
    createBootstrapCheckboxItems(items: ICheckboxItem[], inputHandler: any) {
        const checkboxList: any[] = [];
        const index: number = 0;
        items.forEach(checkboxItem => {
            const item: any = this.createBootstrapCheckboxItem(checkboxItem, index, inputHandler);
            checkboxList.push(item);
        });
        return checkboxList;
    },
    createBootstrapSelectOption(option: any) {
        return(
            <option key={option.id} value={option.value}>{option.label}</option>
          );
    },
    createBootstrapSelect(items?: any[]) {
        const optionsList: any[] = [];
        if (items !== undefined) {
            items.forEach(element => {
                optionsList.push(this.createBootstrapSelectOption(element));
            });
        }
        return optionsList;
    },
    createGuid() {
        return Guid.create().toString();
    },
    capitalise(value: string) {
        return value.charAt(0).toUpperCase() + value.slice(1);
    },
    resolvedFieldName(value: string) {
        const field: string = value.split('.')[2];
        return this.capitalise(field);
    },
    getCapitalisedValues(values: any) {
        let result: string = '';
        values.forEach((value: string) => {
            const resolved: string = result.length === 0 
                ? this.capitalise(value)
                : ',' + this.capitalise(value);
            result = result.concat(resolved);
        });
        return result;
    },
    isSelected(values: any, fieldName: string) {
        let result: boolean = false;
        if (values === undefined || !(values instanceof Array)) return false;
        values.forEach((value: string) => {
            if (value.toLowerCase() === fieldName.toLowerCase()) {
                result = true;
            }
        });
        return result;
    }
};

export default UtilityService;