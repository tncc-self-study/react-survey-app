import DataService from '../services/dataService';
import NotificationService from '../services/toastService';

import { ISalesRep } from '../contracts/ISalesRep';

const SalesRepService = {
    fetchReps() {
        return DataService.get('salesreps')
                   .then((data: any) => {
                       const reps: ISalesRep[] = data;
                       return reps;
                    })
                   .catch((error: any) => {
                       NotificationService.error(error.message);
                   })
    },
    fetchRepValues() {
        return this.fetchReps()
                   .then((data: any) => {
                        const reps: ISalesRep[] = data;
                        const result: string[] = [];
                        reps.forEach((rep: ISalesRep) => {
                            result.push(rep.value);
                        });
                        return result;
                   })
    },
    getRepValues(reps: ISalesRep[]) {
        const result: string[] = [];
        reps.forEach((rep: ISalesRep) => {
            result.push(rep.value);
        });
        return result;
    },
    fetchRepDisplayText() {
        return this.fetchReps()
                   .then((data: any) => {
                        const reps: ISalesRep[] = data;
                        const result: string[] = [];
                        reps.forEach((rep: ISalesRep) => {
                            result.push(rep.label);
                        });
                        return result;
                   })
    },
    getRepDisplayText(reps: ISalesRep[]) {
        const result: string[] = [];
        reps.forEach((rep: ISalesRep) => {
            result.push(rep.label);
        });
        return result;
    }
};

export default SalesRepService;