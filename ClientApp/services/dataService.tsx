import wretch from 'wretch';
import AuthService from '../services/authService';

const apiUrlConfig: any = require('serverApiUrl');

const baseWretch = wretch(apiUrlConfig.serverUrl)
                   .content('application/json')
                   .accept('application/json');

const AuthToken = {
    dummyUser: 'I_AM_ROOT', // !!!! NOT FOR PRODUCTION USE - RECEIVE THIS FROM CLIENT INPUT
    dummyPass: 'I_AM_ROOT', // !!!! NOT FOR PRODUCTION USE - RECEIVE THIS FROM CLIENT INPUT
    
    get() {
        AuthService.logOut();
        return AuthService.login(this.dummyUser, this.dummyPass)
                   .then((data: any) => {
                       var auth = JSON.parse(data);
                       AuthService.setAuthToken(auth.auth_token);
                       return AuthService.getSavedToken();
                    });
        }
}

const DataService = {
    /** 
     * A get request using the http://baseURL/api/{enpoint} returning a json result
     * @param {string} enpoint Endpoint to target on the server 
     * */
    get<T>(endpoint: string) {
        return AuthToken.get()
                 .then(token => {
                    return baseWretch
                    .url(endpoint)
                    .auth('Bearer ' + token)
                    .get()
                    .json()
                 });
    },

    /** 
     * A post request using the http://baseURL/api/{enpoint} and {object} to send to the server returning a json result 
     * @param {string} enpoint Endpoint to target on the server
     * @param {any} object The object to send to the server
     **/
    post(endpoint: string, object: any) {
        return AuthToken.get()
                 .then(token => { 
                     return baseWretch
                        .url(endpoint)
                        .auth('Bearer ' + token)
                        .post(object)
                        .json()
                 });
    },

    /** 
     * A delete request using the http://baseURL/api/{endpoint}
     * @param {string} enpoint Endpoint to target on the server
     * */
    delete(endpoint: string, args: any) {
        return AuthToken.get()
                 .then(token => { 
                     return baseWretch
                        .url(endpoint)
                        .auth('Bearer ' + token)
                        .delete()
                        .json()
                 });
    }
};

export default DataService;