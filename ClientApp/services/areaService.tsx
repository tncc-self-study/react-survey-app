import DataService from '../services/dataService';
import NotificationService from '../services/toastService';

import { IArea } from '../contracts/IArea';

const AreaService = {
    fetchAreas() {
        return DataService.get('areas')
                   .then((data: any) => {
                       const areas: IArea[] = data;
                       return areas;
                    })
                   .catch((error: any) => {
                       NotificationService.error(error.message);
                   })
    },
    fetchAreaValues() {
        return this.fetchAreas()
                   .then((data: any) => {
                        const areas: IArea[] = data;
                        const result: string[] = [];
                        areas.forEach((area: IArea) => {
                            result.push(area.value);
                        });
                        return result;
                   })
    },
    getAreaValues(areas: IArea[]) {
        const result: string[] = [];
        areas.forEach((area: IArea) => {
            result.push(area.value);
        });
        return result;
    },
    fetchAreaDisplayText() {
        return this.fetchAreas()
                   .then((data: any) => {
                        const areas: IArea[] = data;
                        const result: string[] = [];
                        areas.forEach((area: IArea) => {
                            result.push(area.label);
                        });
                        return result;
                   })
    },
    getAreaDisplayText(areas: IArea[]) {
        const result: string[] = [];
        areas.forEach((area: IArea) => {
            result.push(area.label);
        });
        return result;
    }
};

export default AreaService;