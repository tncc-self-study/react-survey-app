import { Industry } from "../models/Industry";
import { ICheckboxItem } from "../contracts/ICheckboxItem";

import  UtilityService  from './utilityService';

const checkboxItems: ICheckboxItem[] = [];
let schemaCheckboxItems: any;
const checkboxClassName: string = 'flex'; 

const CheckboxItems = {
    create() {
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'transporter', label: 'Transporter', className: checkboxClassName, groupIndex: 1 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'distributor', label: 'Distributor', className: checkboxClassName, groupIndex: 1 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'manufacturer', label: 'Manufacturer', className: checkboxClassName, groupIndex: 1 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'reseller', label: 'Reseller', className: checkboxClassName, groupIndex: 2 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'farmer', label: 'Farmer', className: checkboxClassName, groupIndex: 2 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'earthmoving', label: 'Earthmoving', className: checkboxClassName, groupIndex: 2 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'heavyhaulage', label: 'Heavy Haulage', className: checkboxClassName, groupIndex: 3 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'crossborder', label: 'Cross Border', className: checkboxClassName, groupIndex: 3 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'otherIndustry', label: 'Other', className: checkboxClassName, groupIndex: 3 }) );
    }
  }

const SchemaCheckboxItems = {
    create() {
        schemaCheckboxItems = {
            transporter: { type: 'boolean' , title: 'Transporter', default: false },
            distributor: { type: 'boolean' , title: 'Distributor', default: false },
            manufacturer: { type: 'boolean' , title: 'Manufacturer', default: false },
            reseller: { type: 'boolean' , title: 'Reseller', default: false },
            farmer: { type: 'boolean' , title: 'Farmer', default: false },
            earthmoving: { type: 'boolean' , title: 'Earthmoving', default: false },
            heavyhaulage: { type: 'boolean' , title: 'Heavy Haulage', default: false },
            crossborder: { type: 'boolean' , title: 'Cross Border', default: false },
            otherIndustry: { type: 'boolean' , title: 'Other', default: false }
        };
    }
}

const IndustryService = {

    getNewIndustriesModel() {
        return new Industry();
    },
    getIndustryCheckboxItems() {
        CheckboxItems.create();
        return checkboxItems;
    },
    getIndustryCheckboxSchema() {
        SchemaCheckboxItems.create();
        return schemaCheckboxItems;
    },
    getIndustryCheckboxValues() {
        return [ 'transporter','distributor','manufacturer','reseller','farmer','earthmoving','heavyhaulage','crossborder','otherindustry' ];
    },
    getIndustryCheckboxDisplayText() {
        return [ 'Transporter','Distributor','Manufacturer','Reseller','Farmer','Earth Moving','Heavy Haulage','Cross Border','Other' ];
    }
};

export default IndustryService;
