import { Product } from '../models/Product';
import { ICheckboxItem } from "../contracts/ICheckboxItem";

import  UtilityService  from './utilityService';

const checkboxItems: ICheckboxItem[] = [];
let schemaCheckboxItems: any = undefined;
const checkboxClassName: string = 'flex'; 

const CheckboxItems = {
    create() {
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'mercedes', label: 'Mercedes', className: checkboxClassName, groupIndex: 1 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'freightLiner', label: 'FreightLiner', className: checkboxClassName, groupIndex: 1 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'iveco', label: 'Iveco', className: checkboxClassName, groupIndex: 1 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'truckbodyparts', label: 'Truck Body Parts', className: checkboxClassName, groupIndex: 2 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'man', label: 'MAN', className: checkboxClassName, groupIndex: 2 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'volvo', label: 'Volvo', className: checkboxClassName, groupIndex: 2 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'international', label: 'International', className: checkboxClassName, groupIndex: 3 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'tankers', label: 'Tankers', className: checkboxClassName, groupIndex: 3 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'jonesco', label: 'Jonesco', className: checkboxClassName, groupIndex: 3 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'truckaccessories', label: 'Truck Accessories', className: checkboxClassName, groupIndex: 4 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'scania', label: 'Scania', className: checkboxClassName, groupIndex: 4 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'renault', label: 'Renault', className: checkboxClassName, groupIndex: 4 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'trailers', label: 'Trailers', className: checkboxClassName, groupIndex: 5 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'fueldefend', label: 'Fuel Defend', className: checkboxClassName, groupIndex: 5 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'daf', label: 'DAF', className: checkboxClassName, groupIndex: 5 }) );
        checkboxItems.push( UtilityService.createCheckboxItem({ id: UtilityService.createGuid(), key: 'otherProduct', label: 'Other', className: checkboxClassName, groupIndex: 6 }) );
    }
  }

const SchemaCheckboxItems = {
    create() {
        schemaCheckboxItems = {
            mercedes: { type: 'boolean', title: 'Mercedes', default: false },
            freightLiner: { type: 'boolean', title: 'FreightLiner', default: false },
            iveco: { type: 'boolean', title: 'Iveco', default: false },
            truckbodyparts: { type: 'boolean', title: 'Truck Body Parts', default: false },
            man: { type: 'boolean', title: 'MAN', default: false },
            volvo: { type: 'boolean', title: 'Volvo', default: false },
            international: { type: 'boolean', title: 'International', default: false },
            tankers: { type: 'boolean', title: 'Tankers', default: false },
            jonesco: { type: 'boolean', title: 'Jonesco', default: false },
            truckaccessories: { type: 'boolean', title: 'Truck Accessories', default: false },
            scania: { type: 'boolean', title: 'Scania', default: false },
            renault: { type: 'boolean', title: 'Renault', default: false },
            trailers: { type: 'boolean', title: 'Trailers', default: false },
            fueldefend: { type: 'boolean', title: 'Fuel Defend', default: false },
            daf: { type: 'boolean', title: 'DAF', default: false },
            otherProduct: { type: 'boolean', title: 'Other', default: false }
        };
    }
}

const IndustryService = {

    getNewProductsModel() {
        return new Product();
    },
    getProductCheckboxItems() {
        CheckboxItems.create();
        return checkboxItems;
    },
    getProductCheckboxSchema() {
        SchemaCheckboxItems.create();
        return schemaCheckboxItems;
    },
    getProductCheckboxValues() {
        return [ 'mercedes','freightliner','iveco','truckbodyparts','man','volvo','international','tankers','jonesco','truckaccessories','scania','renault','trailers','fueldefend','daf','otherProduct' ];
    },
    getProductCheckboxDisplayText() {
        return [ 'Mercedes','Freight Liner','Iveco','Truck Body Parts','MAN','Volvo','International','Tankers','Jonesco','Truck Accessories','Scania','Renault','Trailers','Fuel Defend','DAF','Other' ];
    },
};

export default IndustryService;