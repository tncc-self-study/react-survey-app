import { toast, ToastOptions } from 'react-toastify';

const options: ToastOptions = {
    position: toast.POSITION.BOTTOM_RIGHT,
    autoClose: 10000,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: false,
    draggablePercent: 80
};

const NotificationService = {

    info(message: string) {
        toast.info(message, options);
    },
    success(message: string) {
        toast.success(message, options);
    },
    warn(message: string) {
        toast.warn(message, options);
    },
    error(message: string) {
        toast.error(message, options);
    }
}

export default NotificationService;
