import AreaService from '../services/areaService';
import SalesRepService from '../services/repsService';
import ProductService from '../services/productService';
import IndustryService from '../services/industryService';
import UtilityService from './utilityService';
import DataService from './dataService';
import NotificationService from './toastService';

import { NampoModel } from '../models/NampoModel';
import { IArea } from '../contracts/IArea';
import { ISalesRep } from '../contracts/ISalesRep';

const defaultCustomerOptions = {
    email: '',
    mobile: '',
    fax: '',
    telephone: '',
    town: '',
    area: '',
    isCurrentCustomer: false,
    customerName: ''
};

const defaultProductOptions = {
    mercedes: false,
    freightLiner: false,
    iveco: false,
    truckbodyparts: false,
    man: false,
    volvo: false,
    international: false,
    tankers: false,
    jonesco: false,
    truckaccessories: false,
    scania: false,
    renault: false,
    trailers: false,
    fueldefend: false,
    daf: false,
    otherProduct: false
};

const defaultIndustryOptions = {
    transporter: false,
    distributor: false,
    manufacturer: false,
    reseller: false,
    farmer: false,
    earthmoving: false,
    heavyhaulage: false,
    crossborder: false,
    otherIndustry: false,
};

const defaultOtherInfoOptions = {
    comments: '',
    isUrgent: false,
    rep: ''
};

const CustomerSchema = {
    get() {
        return AreaService.fetchAreas()
            .then((data: any) => {
                const areas: IArea[] = data;
                const customer: any = {
                    customerName: { type: 'string', title: 'Customer' },
                    isCurrentCustomer: { type: 'boolean', title: 'Current Customer', default: false },
                    area: { type: 'string', title: 'Area',
                            enum: AreaService.getAreaValues(areas),
                            enumNames: AreaService.getAreaValues(areas)
                    },
                    town: { type: 'string', title: 'City/Town' },
                    telephone: { type: 'string', title: 'Phone', minLength: 10 },
                    fax: { type: 'string', title: 'Fax', minLength: 10 },
                    mobile: { type: 'string', title: 'Cell', minLength: 10 },
                    email: { type: 'string', format: 'email', title: 'Email' }
                }
                return customer;
            });
    }
};

const OtherInformationSchema = {
    get() {
        return SalesRepService.fetchReps()
            .then((data: any) => {
                const reps: ISalesRep[] = data;
                const info: any = {
                    comments: { type: 'string', title: 'Comment' },
                    isUrgent: { type: 'boolean', title: 'Urgent', default: false },
                    rep: { type: 'string', title: 'Survey Taker',
                            enum: SalesRepService.getRepValues(reps),
                            enumNames: SalesRepService.getRepDisplayText(reps)
                        }
                    }
                return info;
            });
    }
};

const NampoService = {
    getModelCollections() {
        return Promise.all([AreaService.fetchAreas(), SalesRepService.fetchReps()])
            .then(function(results) {
                return results;
            });
    },
    getNewNampoModel() {
        const model: NampoModel = new NampoModel();
        model.email = defaultCustomerOptions.email;
        model.mobile = defaultCustomerOptions.mobile;
        model.fax = defaultCustomerOptions.fax;
        model.telephone = defaultCustomerOptions.telephone;
        model.town = defaultCustomerOptions.town;
        model.area = defaultCustomerOptions.area;
        model.isCurrentCustomer = defaultCustomerOptions.isCurrentCustomer;
        model.customerName = defaultCustomerOptions.customerName;
        model.mercedes = defaultProductOptions.mercedes;
        model.freightLiner = defaultProductOptions.freightLiner;
        model.iveco = defaultProductOptions.iveco;
        model.truckbodyparts = defaultProductOptions.truckbodyparts;
        model.man = defaultProductOptions.man;
        model.volvo = defaultProductOptions.volvo;
        model.international = defaultProductOptions.international;
        model.tankers = defaultProductOptions.tankers;
        model.jonesco = defaultProductOptions.jonesco;
        model.truckaccessories = defaultProductOptions.truckaccessories;
        model.scania = defaultProductOptions.scania;
        model.renault = defaultProductOptions.renault;
        model.trailers = defaultProductOptions.trailers;
        model.fueldefend = defaultProductOptions.fueldefend;
        model.daf = defaultProductOptions.daf;
        model.otherProduct = defaultProductOptions.otherProduct;
        model.transporter = defaultIndustryOptions.transporter;
        model.distributor = defaultIndustryOptions.distributor;
        model.manufacturer = defaultIndustryOptions.manufacturer;
        model.reseller = defaultIndustryOptions.reseller;
        model.farmer = defaultIndustryOptions.farmer;
        model.earthmoving = defaultIndustryOptions.earthmoving;
        model.heavyhaulage = defaultIndustryOptions.heavyhaulage;
        model.crossborder = defaultIndustryOptions.crossborder;
        model.otherIndustry = defaultIndustryOptions.otherIndustry;
        model.comments = defaultOtherInfoOptions.comments;
        model.isUrgent = defaultOtherInfoOptions.isUrgent;
        model.rep = defaultOtherInfoOptions.rep;

        return model;
    },
    getNewJsonNampoModel() {
        const model = {
            "email": defaultCustomerOptions.email,
            "mobile": defaultCustomerOptions.mobile,
            "fax": defaultCustomerOptions.fax,
            "telephone": defaultCustomerOptions.telephone,
            "town": defaultCustomerOptions.town,
            "area": defaultCustomerOptions.area,
            "isCurrentCustomer": defaultCustomerOptions.isCurrentCustomer,
            "customerName": defaultCustomerOptions.customerName,
            "mercedes": defaultProductOptions.mercedes,
            "freightLiner": defaultProductOptions.freightLiner,
            "iveco": defaultProductOptions.iveco,
            "truckbodyparts": defaultProductOptions.truckbodyparts,
            "man": defaultProductOptions.man,
            "volvo": defaultProductOptions.volvo,
            "international": defaultProductOptions.international,
            "tankers": defaultProductOptions.tankers,
            "jonesco": defaultProductOptions.jonesco,
            "truckaccessories": defaultProductOptions.truckaccessories,
            "scania": defaultProductOptions.scania,
            "renault": defaultProductOptions.renault,
            "trailers": defaultProductOptions.trailers,
            "fueldefend": defaultProductOptions.fueldefend,
            "daf": defaultProductOptions.daf,
            "otherProduct": defaultProductOptions.otherProduct,
            "transporter": defaultIndustryOptions.transporter,
            "distributor": defaultIndustryOptions.distributor,
            "manufacturer": defaultIndustryOptions.manufacturer,
            "reseller": defaultIndustryOptions.reseller,
            "farmer": defaultIndustryOptions.farmer,
            "earthmoving": defaultIndustryOptions.earthmoving,
            "heavyhaulage": defaultIndustryOptions.heavyhaulage,
            "crossborder": defaultIndustryOptions.crossborder,
            "otherIndustry": defaultIndustryOptions.otherIndustry,
            "comments": defaultOtherInfoOptions.comments,
            "isUrgent": defaultOtherInfoOptions.isUrgent,
            "rep": defaultOtherInfoOptions.rep,
        };

        return model;
    },
    getSchemaProperties() {
        return Promise.all([CustomerSchema.get(), OtherInformationSchema.get()])
            .then((data :any) => {
                const customerSchema = data[0];
                const otherInfoSchema = data[1];

                const wrapper: any = {
                    customer: {
                        title: 'Customer Details',
                        type: 'object',
                        properties: customerSchema,
                        required: [ "customerName","area","town","email" ]
                    },
                    products: {
                        title: 'Products of Interest',
                        type: 'array',
                        items: {
                            type: 'string',
                            enum: ProductService.getProductCheckboxValues(),
                            enumNames: ProductService.getProductCheckboxDisplayText()
                        },
                        uniqueItems: true
                    },
                    industries: {
                        title: 'Industry',
                        type: 'array',
                        items: {
                            type: 'string',
                            enum: IndustryService.getIndustryCheckboxValues(),
                            enumNames: IndustryService.getIndustryCheckboxDisplayText()
                        },
                        uniqueItems: true
                    },
                    otherInfo: {
                        title: 'Additional Information',
                        type: 'object',
                        properties: otherInfoSchema,
                        required: [ "rep" ]
                    }
                }

                return wrapper;
            });
    },
    getUiSchema() {
        const uiSchema: any = {
            "customer": {
                "classNames": "group__border",
                "ui:order": [
                    "customerName",
                    "isCurrentCustomer",
                    "area",
                    "town",
                    "telephone",
                    "fax",
                    "mobile",
                    "email"
                ],
                "customerName": {
                    "ui:autofocus": true
                },
                "area": {
                    "ui:widget": "select"
                }
            },
            "products": {
                "classNames": "group__border",
                "ui:widget": "checkboxes",
                "ui:options": {
                    inline: true
                }
            },
            "industries": {
                "classNames": "group__border",
                "ui:widget": "checkboxes",
                "ui:options": {
                    inline: true
                }
            },
            "otherInfo": {
                "classNames": "group__border",
                "ui:order": [
                    "comments",
                    "isUrgent",
                    "rep",
                ],
                "rep": {
                    "ui:widget": "select"
                },
                "comments": {
                    "ui:widget": "textarea"
                }
            }
        };

        return uiSchema;
    },
    getModelToSubmit(formData: any) {
        return {
            customerName: formData.customer.customerName,
            isCurrentCustomer: formData.customer.isCurrentCustomer,
            area: formData.customer.area,
            town: formData.customer.town,
            telephone: formData.customer.telephone,
            fax: formData.customer.fax,
            mobile: formData.customer.mobile,
            email: formData.customer.email,
            
            mercedes: UtilityService.isSelected(formData.products, 'mercedes'),
            freightliner: UtilityService.isSelected(formData.products, 'freightLiner'),
            iveco: UtilityService.isSelected(formData.products, 'iveco'),
            truckbodyparts: UtilityService.isSelected(formData.products, 'truckbodyparts'),
            man: UtilityService.isSelected(formData.products, 'man'),
            volvo: UtilityService.isSelected(formData.products, 'volvo'),
            international: UtilityService.isSelected(formData.products, 'international'),
            tankers: UtilityService.isSelected(formData.products, 'tankers'),
            jonesco: UtilityService.isSelected(formData.products, 'jonesco'),
            truckaccessories: UtilityService.isSelected(formData.products, 'truckaccessories'),
            scania: UtilityService.isSelected(formData.products, 'scania'),
            renault: UtilityService.isSelected(formData.products, 'renault'),
            trailers: UtilityService.isSelected(formData.products, 'trailers'),
            fueldefend: UtilityService.isSelected(formData.products, 'fueldefend'),
            daf: UtilityService.isSelected(formData.products, 'daf'),
            otherproduct: UtilityService.isSelected(formData.products, 'otherProduct'),

            transporter: UtilityService.isSelected(formData.industries, 'transporter'),
            distributor: UtilityService.isSelected(formData.industries, 'distributor'),
            manufacturer: UtilityService.isSelected(formData.industries, 'manufacturer'),
            reseller: UtilityService.isSelected(formData.industries, 'reseller'),
            farmer: UtilityService.isSelected(formData.industries, 'farmer'),
            earthmoving: UtilityService.isSelected(formData.industries, 'earthmoving'),
            heavyhaulage: UtilityService.isSelected(formData.industries, 'heavyhaulage'),
            crossborder: UtilityService.isSelected(formData.industries, 'crossborder'),
            otherindustry: UtilityService.isSelected(formData.industries, 'otherIndustry'),
            
            comments: formData.otherInfo.comments,
            isurgent: formData.otherInfo.isUrgent,
            rep: formData.otherInfo.rep
        }
    },
    showSubmitErrors(errors: any) {
        errors.forEach((error: any) => {
            const message: string = error.message;
            const field: string = UtilityService.resolvedFieldName(error.property);
            const errorMessage: string = field + ' ' + message;
            NotificationService.error(errorMessage);
        });
    },
    save(data: any) {
        const model: any = NampoService.getModelToSubmit(data);

        return DataService.post('survey/save', model)
                   .then((data: any) => {
                       if (data.status === 'failed') {
                            NotificationService.warn(data.message);
                            return { status: false };
                       } else {
                            NotificationService.success('Thank you for your time and feedback');
                            return { status: true };
                       }
                    })
                   .catch((error: any) => {
                       NotificationService.error(error.message);
                       return { status: false };
                   })
    }
};

export default NampoService;