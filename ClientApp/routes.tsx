import * as React from 'react';
import { Route } from 'react-router-dom';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import { AreaData } from './components/AreaData';
import { Nampo } from './components/Nampo';
import { ToDo } from './components/Todo';

export const routes = <Layout>
    <Route exact path='/' component={ Home } />
    <Route path='/counter' component={ Counter } />
    <Route path='/fetchdata' component={ FetchData } />
    <Route path='/areas' component={ AreaData } />
    <Route path='/nampo' component={ Nampo } />
    <Route path='/todo' component={ ToDo } />
</Layout>;
