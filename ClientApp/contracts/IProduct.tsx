export interface IProduct {
    mercedes?: boolean;
    freightLiner?: boolean;
    iveco?: boolean;
    truckbodyparts?: boolean;
    man?: boolean;
    volvo?: boolean;
    international?: boolean;
    tankers?: boolean;
    jonesco?: boolean;
    truckaccessories?: boolean;
    scania?: boolean;
    renault?: boolean;
    trailers?: boolean;
    fueldefend?: boolean;
    daf?: boolean;
    otherProduct?: boolean;
  }