export interface IArea {
    id: string;
    label: string;
    value: string;
}