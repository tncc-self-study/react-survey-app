export interface ICheckboxItem {
    id: string;
    key: string;
    label: string;
    className: string;
    groupIndex: number;
  }