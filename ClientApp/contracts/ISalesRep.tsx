export interface ISalesRep {
    id: string;
    label: string;
    value: string;
}