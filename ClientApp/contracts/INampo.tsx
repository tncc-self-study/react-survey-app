import { ICustomer } from './icustomer';
import { IProduct } from './iproduct';
import { IIndustry } from './iindustry';
import { IOtherInfo } from './iotherinfo';

export interface INampo extends ICustomer, IProduct, IIndustry, IOtherInfo {
}