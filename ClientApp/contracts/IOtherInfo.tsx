export interface IOtherInfo {
    comments?: string;
    isUrgent?: boolean;
    rep?: string;
  }