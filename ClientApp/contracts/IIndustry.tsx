export interface IIndustry {
    transporter?: boolean;
    distributor?: boolean;
    manufacturer?: boolean;
    reseller?: boolean;
    farmer?: boolean;
    earthmoving?: boolean;
    heavyhaulage?: boolean;
    crossborder?: boolean;
    otherIndustry?: boolean;
  }