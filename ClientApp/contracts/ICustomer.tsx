export interface ICustomer {
    email?: string;
    mobile?: string;
    fax?: string;
    telephone?: string;
    town?: string;
    area?: string;
    isCurrentCustomer?: boolean;
    customerName?: string;
  }