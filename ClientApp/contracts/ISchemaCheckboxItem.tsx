export interface ISchemaItem {
    fieldName: string;
    properties: ISchemaItemProperties;
  }

  export interface ISchemaItemProperties {
    type: string;
    title: string;
    defaultValue: any;
  }