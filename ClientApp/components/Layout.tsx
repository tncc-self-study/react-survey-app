import * as React from 'react';
import { NavMenu } from './NavMenu';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Footer from './Footer';

export interface LayoutProps {
    children?: React.ReactNode;
}

export class Layout extends React.Component<LayoutProps, {}> {
    public render() {
        return <div className="site">
            <NavMenu />
            <div className='site-content container-fluid'>
                { this.props.children }
                <ToastContainer />
            </div>
            <Footer />
        </div>;
    }
}
