import * as React from 'react'
import { RouteComponentProps } from 'react-router';

import UtilityService from '../services/utilityService';
import NampoService from '../services/nampoService';
import ProductService from '../services/productService';
import IndustryService from '../services/industryService';

import { ICheckboxItem } from '../contracts/ICheckboxItem';

interface NampoDataState {
    loading: boolean;
    model: any;
}

export class Nampo extends React.Component<RouteComponentProps<{}>, NampoDataState> {
  
    private areaSelect: any;
    private repSelect: any;
    private productChkItems: any;
    private industryChkItems: any;

    constructor(props: any) {
        super(props);
        this.state = { 
            loading: true,
            model: NampoService.getNewJsonNampoModel()
        };
    }
  
    componentDidMount() {
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        
        NampoService.getModelCollections()
                    .then((data: any) => {
                        const areasData = data[0];
                        const repsData = data[1];
                        this.initialiseCollections(areasData, repsData);
                        this.setState({ 
                            loading: false 
                        });
                    });
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderSurvey();

        return <div>
            <h1>Nampo Survey</h1>
            { contents }
        </div>
    }

    handleInputChange(event: any) {
        const target: any = event.target;
        const value: any = target.type === 'checkbox' ? target.checked : target.value;
        const name: string = target.name;
        const parsedJson = JSON.parse(JSON.stringify(this.state.model)); // Create deep clone
        parsedJson[name] = value; // update model

        this.setState((prevState, props) => ({
            model: parsedJson
          }));
    }

    handleSubmit(event: any) {
        console.log('A survey was submitted: ' + JSON.stringify(this.state));
        event.preventDefault();
    }

    private initialiseCollections(areasData: any, repsData: any) {
        this.areaSelect = UtilityService.createBootstrapSelect(areasData);
        this.repSelect = UtilityService.createBootstrapSelect(repsData);

        const pChkItems: ICheckboxItem[] = ProductService.getProductCheckboxItems();
        this.productChkItems = UtilityService.createBootstrapCheckboxItems(pChkItems, this.handleInputChange);
        
        const iChkItems: ICheckboxItem[] = IndustryService.getIndustryCheckboxItems();
        this.industryChkItems = UtilityService.createBootstrapCheckboxItems(iChkItems, this.handleInputChange);
    }

    private getCustomerDetails() {
        return (<div className="card">
                    <div className="card-header"><h2>Customer Details</h2></div>
                    <div className="card-body">
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Customer Name" name="customerName" key="customerName" 
                                   value={this.state.model.customerName} required={true} 
                                   onChange={this.handleInputChange} />
                        </div>
                        <div className="form-group form-check">
                            <label className="form-check-label">
                                <input className="form-check-input" type="checkbox" name="isCurrentCustomer" key="isCurrentCustomer" 
                                       checked={this.state.model.isCurrentCustomer} onChange={this.handleInputChange} /> Current Customer
                            </label>
                        </div>
                        <div className="form-group">
                            <select key="area" name="area" className="form-control custom-select-sm" onChange={this.handleInputChange} value={this.state.model.area}>
                                <option value="">Select Area</option>
                                { this.areaSelect }
                            </select>
                        </div>
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="City/Town" name="town" key="town" value={this.state.model.town} required={true} 
                                   onChange={this.handleInputChange} />
                        </div>
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Telephone" name="telephone" key="telephone" value={this.state.model.telephone} 
                                   onChange={this.handleInputChange} />
                        </div>
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Fax" name="fax" key="fax" value={this.state.model.fax} 
                                   onChange={this.handleInputChange} />
                        </div>
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Cellphone" name="mobile" key="mobile" value={this.state.model.mobile} 
                                   onChange={this.handleInputChange} />
                        </div>
                        <div className="form-group">
                            <input type="email" className="form-control" placeholder="Email" name="email" key="email" value={this.state.model.email} required={true} 
                                   onChange={this.handleInputChange} />
                        </div>
                    </div> 
                </div>);
    }

    private getProductDetails() {
        return (<div className="card">
                    <div className="card-header"><h2>Products of Interest</h2></div>
                    <div className="card-body">
                        { this.productChkItems }
                    </div> 
                </div>);
    }

    private getIndustryDetails() {
        return (<div className="card">
                    <div className="card-header"><h2>Types of Industry</h2></div>
                    <div className="card-body">
                        { this.industryChkItems }
                    </div> 
                </div>);
    }

    private getOtherInfoDetails() {
        return (<div className="card">
                    <div className="card-header"><h2>Other Information</h2></div>
                    <div className="card-body">
                        <div className="form-group">
                            <label>Comment:</label>
                            <textarea className="form-control" rows={5} name="comments" key="comments" value={this.state.model.comments} onChange={this.handleInputChange} />
                        </div>
                        <div className="form-group form-check">
                            <label className="form-check-label">
                                <input className="form-check-input" type="checkbox" name="isUrgent" key="isUrgent" checked={this.state.model.isUrgent} onChange={this.handleInputChange} /> Urgent
                            </label>
                        </div>
                        <div className="form-group">
                            <select key="rep" name="rep" className="form-control custom-select-sm" onChange={this.handleInputChange} value={this.state.model.rep}>
                                <option value="">Select Survey Taker</option>
                                { this.repSelect }
                            </select>
                        </div>
                    </div> 
                </div>);
    }

    private getFormBody() {
        return (<table className='table'>
                    <tbody>
                        <tr>
                            <td>
                                { this.getCustomerDetails() }
                            </td>
                        </tr>
                        <tr>
                            <td>
                                { this.getProductDetails() }
                            </td>
                        </tr>
                        <tr>
                            <td>
                                { this.getIndustryDetails() }
                            </td>
                        </tr>
                        <tr>
                            <td>
                                { this.getOtherInfoDetails() }
                            </td>
                        </tr>
                    </tbody>
                </table>);
    }

    private renderSurvey() {
        return (<div className="content-wrapper"> 
            <form onSubmit={this.handleSubmit}>
                { this.getFormBody() }
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        </div>);
    }
}
