import * as React from 'react';
import { Link, NavLink } from 'react-router-dom';

export class NavMenu extends React.Component<{}, {}> {
    public render() {
        return <div className='main-nav'>
                <div className='navbar navbar-inverse navbar-fixed-top'>
                    <div className='navbar-header'>
                        <button type='button' className='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>
                            <span className='sr-only'>Toggle navigation</span>
                            <span className='icon-bar'></span>
                            <span className='icon-bar'></span>
                            <span className='icon-bar'></span>
                        </button>
                        <Link className='navbar-brand' to={ '/' }>SURVEY</Link>
                    </div>
                    <div className='navbar-collapse collapse'>
                        <ul className='nav navbar-nav'>
                            <li>
                                <NavLink to={ '/' } exact activeClassName='active'>
                                    <span className='glyphicon glyphicon-home'></span> Home
                                </NavLink>
                            </li>
                            
                            <li>
                                <div className="btn-group">
                                    <button type="button" className="btn btn-inverse">
                                        <span className='glyphicon glyphicon-education'></span> Boiler Plate
                                    </button>
                                    <button type="button" className="btn btn-inverse dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span className="caret"></span>
                                        <span className="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul className="dropdown-menu">
                                    <li>
                                        <NavLink to={ '/counter' } activeClassName='active'>
                                            <span className='glyphicon glyphicon-plus'></span> Counter
                                        </NavLink>
                                    </li>
                                    <li>
                                        <NavLink to={ '/fetchdata' } activeClassName='active'>
                                            <span className='glyphicon glyphicon-ok'></span> Fetch data
                                        </NavLink>
                                    </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <div className="btn-group">
                                    <button type="button" className="btn btn-inverse">
                                        <span className='glyphicon glyphicon-education'></span> TNCC
                                    </button>
                                    <button type="button" className="btn btn-inverse dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span className="caret"></span>
                                        <span className="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul className="dropdown-menu">
                                        <li>
                                            <NavLink to={ '/areas' } activeClassName='active'>
                                                <span className='glyphicon glyphicon-th-list'></span> Fetch Areas
                                            </NavLink>
                                        </li>
                                        <li>
                                            <NavLink to={ '/nampo' } activeClassName='active'>
                                                <span className='glyphicon glyphicon-check'></span> Nampo Survey
                                            </NavLink>
                                        </li>
                                        <li>
                                            <NavLink to={ '/todo' } activeClassName='active'>
                                                <span className='glyphicon glyphicon-ok-circle'></span> To Do
                                            </NavLink>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
        </div>;
    }
}
