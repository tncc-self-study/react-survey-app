import * as React from 'react';

interface FooterState {
    year: number;
}

class Footer extends React.Component<{}, FooterState> {
  constructor(props: any) {
    super(props);
    this.state = {
        year: new Date().getFullYear()
    };
  }

  componentWillMount() {
    this.setState({
        year: new Date().getFullYear()
    });
  }

  render() {
    return (
      <div className="site-footer">
        <p className="site-footer-content">&copy; { this.state.year } <a className="site-footer-anchor" href="http://www.tncconsultant.com" target="_blank"> TNC Consultant</a></p>
      </div>
    );
  }
}

export default Footer;