import * as React from 'react';
import { RouteComponentProps } from 'react-router';
import AreaService from '../services/areaService';
import { IArea } from '../contracts/IArea';

interface FetchDataAreaState {
    areas: IArea[];
    loading: boolean;
}

export class AreaData extends React.Component<RouteComponentProps<{}>, FetchDataAreaState> {
    constructor(props: any) {
        super(props);
        this.state = { areas: [], loading: true };
        const ctx: any = this;
        AreaData.fetchAreas(ctx);
    }

    public render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : AreaData.renderAreasTable(this.state.areas);

        return <div>
            <h1>Areas</h1>
            { contents }
        </div>;
    }

    private static renderAreasTable(areas: IArea[]) {
        return <table className='table'>
            <thead>
                <tr key="area-header">
                    <th>Description</th>
                </tr>
            </thead>
            <tbody>
            {areas.map(area =>
                <tr key={ area.id }>
                    <td>{ area.label }</td>
                </tr>
            )}
            </tbody>
        </table>;
    }

    private static fetchAreas(ctx: any) {
        AreaService.fetchAreas()
                   .then((data: any) => {
                        ctx.setState({ areas: data, loading: false });
                   })
    }
}
