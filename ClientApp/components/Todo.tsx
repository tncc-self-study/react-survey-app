import * as React from 'react'
import { RouteComponentProps } from 'react-router';
import Form from "react-jsonschema-form";

import NampoService from '../services/nampoService';

interface ToDoDataState {
    key: number;
    loading: boolean;
    schema?: any;
    uiSchema?: any;
}

export class ToDo extends React.Component<RouteComponentProps<{}>, ToDoDataState> {
  
    constructor(props: any) {
        super(props);
        this.handleReset();

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {
        let contents = this.state.loading
            ? <p><em>Loading...</em></p>
            : this.renderSurvey();

        return <div> {contents} </div>;
    }

    private renderSurvey() {
        return <Form key={this.state.key}
                schema={this.state.schema} 
                uiSchema={this.state.uiSchema}                
                onSubmit={this.handleSubmit}
                showErrorList={false}
                noHtml5Validate={true} />
    }

    private handleSubmit(submit: any) {
        NampoService.save(submit.formData)
            .then((result: any) => {
                if (result.status) this.props.history.push('/todo');
            });
    }

    /*
    private handleChange(change: any) {
        // onChange={this.handleChange}
        const data = change.formData;
    }

    private handleError(errors: any) {
        // onError={this.handleError}
        NampoService.showSubmitErrors(errors);
    }
    */
   
    private setLoadingState() {
        this.state = { 
            key: Date.now(),
            loading: true
        };
    }

    private setLoadedState() {
        NampoService.getSchemaProperties()
            .then((data: any) => {
                const newSchema: any = {
                    title: 'Nampo Survey',
                    type: 'object',
                    properties: data
                }
                this.setState({
                    schema: newSchema,
                    uiSchema: NampoService.getUiSchema(),
                    loading: false
                })
            });
    }

    private handleReset() {
        this.setLoadingState();
        this.setLoadedState();
    }
}
