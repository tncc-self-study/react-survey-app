import { IIndustry } from '../contracts/iindustry';

export class Industry implements IIndustry {
  transporter?: boolean;
  distributor?: boolean;
  manufacturer?: boolean;
  reseller?: boolean;
  farmer?: boolean;
  earthmoving?: boolean;
  heavyhaulage?: boolean;
  crossborder?: boolean;
  otherIndustry?: boolean;

  defaultOptions = {
    transporter: false,
    distributor: false,
    manufacturer: false,
    reseller: false,
    farmer: false,
    earthmoving: false,
    heavyhaulage: false,
    crossborder: false,
    otherIndustry: false,
  };

  constructor() {
      this.transporter = this.defaultOptions.transporter;
      this.distributor = this.defaultOptions.distributor;
      this.manufacturer = this.defaultOptions.manufacturer;
      this.reseller = this.defaultOptions.reseller;
      this.farmer = this.defaultOptions.farmer;
      this.earthmoving = this.defaultOptions.earthmoving;
      this.heavyhaulage = this.defaultOptions.heavyhaulage;
      this.crossborder = this.defaultOptions.crossborder;
      this.otherIndustry = this.defaultOptions.otherIndustry;
  }
}