import { IOtherInfo } from '../contracts/iotherinfo';

export class OtherInfo implements IOtherInfo {
  comments?: string;
  isUrgent?: boolean;
  rep?: string;

  defaultOptions = {
    comments: '',
    isUrgent: false,
    rep: ''
  };

  constructor() {
      this.comments = this.defaultOptions.comments;
      this.isUrgent = this.defaultOptions.isUrgent;
      this.rep = this.defaultOptions.rep;
  }
}