import { ICustomer } from '../contracts/icustomer';

export class Customer implements ICustomer {
  email?: string;
  mobile?: string;
  fax?: string;
  telephone?: string;
  town?: string;
  area?: string;
  isCurrentCustomer?: boolean;
  customerName?: string;

  defaultOptions: ICustomer = {
    email: '',
    mobile: '',
    fax: '',
    telephone: '',
    town: '',
    area: '',
    isCurrentCustomer: false,
    customerName: ''
  };

  constructor() {
    this.email = this.defaultOptions.email;
    this.mobile = this.defaultOptions.mobile;
    this.fax = this.defaultOptions.fax;
    this.telephone = this.defaultOptions.telephone;
    this.town = this.defaultOptions.town;
    this.area = this.defaultOptions.area;
    this.isCurrentCustomer = this.defaultOptions.isCurrentCustomer;
    this.customerName = this.defaultOptions.customerName;
  }
}