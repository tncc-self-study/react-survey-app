import { INampo } from '../contracts/inampo';

export class NampoModel implements INampo {
  // Customer options
  email?: string;
  mobile?: string;
  fax?: string;
  telephone?: string;
  town?: string;
  area?: string;
  isCurrentCustomer?: boolean;
  customerName?: string;
  // Products of interest
  mercedes?: boolean;
  freightLiner?: boolean;
  iveco?: boolean;
  truckbodyparts?: boolean;
  man?: boolean;
  volvo?: boolean;
  international?: boolean;
  tankers?: boolean;
  jonesco?: boolean;
  truckaccessories?: boolean;
  scania?: boolean;
  renault?: boolean;
  trailers?: boolean;
  fueldefend?: boolean;
  daf?: boolean;
  otherProduct?: boolean;
  // Types of industry
  transporter?: boolean;
  distributor?: boolean;
  manufacturer?: boolean;
  reseller?: boolean;
  farmer?: boolean;
  earthmoving?: boolean;
  heavyhaulage?: boolean;
  crossborder?: boolean;
  otherIndustry?: boolean;
  // Other information
  comments?: string;
  isUrgent?: boolean;
  rep?: string;

  constructor() {}

  toJson() {
    return JSON.stringify(this);
  }

  static fromJSON(json: string): NampoModel {
    let model: NampoModel = Object.create(NampoModel.prototype);
    return Object.assign(model, json, {});
  }

  static reviver(key: string, value: any): any {
    return key === "" ? NampoModel.fromJSON(value) : value;
  }

  clone() {
    const json = this.toJson();
    return NampoModel.fromJSON(json);
  }

}