import { IProduct } from '../contracts/iproduct';

export class Product implements IProduct {
  mercedes?: boolean;
  freightLiner?: boolean;
  iveco?: boolean;
  truckbodyparts?: boolean;
  man?: boolean;
  volvo?: boolean;
  international?: boolean;
  tankers?: boolean;
  jonesco?: boolean;
  truckaccessories?: boolean;
  scania?: boolean;
  renault?: boolean;
  trailers?: boolean;
  fueldefend?: boolean;
  daf?: boolean;
  otherProduct?: boolean;

  defaultOptions: IProduct = {
    mercedes: false,
    freightLiner: false,
    iveco: false,
    truckbodyparts: false,
    man: false,
    volvo: false,
    international: false,
    tankers: false,
    jonesco: false,
    truckaccessories: false,
    scania: false,
    renault: false,
    trailers: false,
    fueldefend: false,
    daf: false,
    otherProduct: false
  };

  constructor() {
      this.mercedes = this.defaultOptions.mercedes;
      this.freightLiner = this.defaultOptions.freightLiner;
      this.iveco = this.defaultOptions.iveco;
      this.truckbodyparts = this.defaultOptions.truckbodyparts;
      this.man = this.defaultOptions.man;
      this.volvo = this.defaultOptions.volvo;
      this.international = this.defaultOptions.international;
      this.tankers = this.defaultOptions.tankers;
      this.jonesco = this.defaultOptions.jonesco;
      this.truckaccessories = this.defaultOptions.truckaccessories;
      this.scania = this.defaultOptions.scania;
      this.renault = this.defaultOptions.renault;
      this.trailers = this.defaultOptions.trailers;
      this.fueldefend = this.defaultOptions.fueldefend;
      this.daf = this.defaultOptions.daf;
      this.otherProduct = this.defaultOptions.otherProduct;
  }
}