using System;
using System.Threading.Tasks;
using react_survey_app.Data.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using react_survey_app.Contracts;
using react_survey_app.Data.Infrastructure;

namespace react_survey_app.Controllers
{
    [Authorize]
    public class NampoController : Controller
    {
        ILogger<NampoController> _logger;
        IRepository _repository;

        public NampoController(ILogger<NampoController> logger,
                               IRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [Route("api/survey/save")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]NampoSurvey survey)
        {
            _logger.LogDebug($"Receive survey {nameof(NampoController)}");

            try {
              await _repository.InsertAsync(survey);
              var response = new { status = "success", message = "Inserted this survey" };
              return Ok(response);
            }
            catch (Exception ex) {
              _logger.LogDebug($"Failed to insert survey. {ex.Message}");
              var response = new { status = "fail", message = "Failed to insert this survey" };
              return BadRequest(response);
            }
        }
    }
}
