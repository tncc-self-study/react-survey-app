using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using react_survey_app.Auth;
using react_survey_app.Data.Entities;
using react_survey_app.Models;

namespace react_survey_app.Controllers
{
    public class AuthController: Controller
    {
        ILogger<AuthController> _logger;
        readonly IJwtFactory _jwtFactory;
        readonly JwtIssuerOptions _jwtOptions;

        public AuthController(IJwtFactory jwtFactory,
                              IOptions<JwtIssuerOptions> jwtOptions,
                              ILogger<AuthController> logger) {
            _jwtFactory = jwtFactory;
            _jwtOptions = jwtOptions.Value;
            _logger = logger;
        }

        [Route("api/survey/login")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]AuthModel model) {
            if (!ModelState.IsValid) {
              _logger.LogError("Invalid credentials supplied");
              return BadRequest("Login Failure");
            }
            if (!await ValidCredentialsAsync(model.Username, model.Password))
            {
              _logger.LogError("Invalid credentials supplied");
              return BadRequest("Login Failure");
            }

            var identity = await GetClaimsIdentityAsync(model.Username, model.Password);
            if (identity == null)
            {
              _logger.LogError("Failed to create claims identity");
              return BadRequest("Login Failure");
            }

          var jwt = await TokensHelper.GenerateJwtAsync(identity, _jwtFactory, model.Username, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });
          _logger.LogInformation("Successfully generated token"); // Could perform other login related actions here....
          return new OkObjectResult(jwt);
        }
        async Task<ClaimsIdentity> GetClaimsIdentityAsync(string userName, string password) {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return await Task.FromResult<ClaimsIdentity>(null);

            // get the dummy user to verifty
            var userToVerify = await GetDummyUser(userName, password);
            if (userToVerify == null) return await Task.FromResult<ClaimsIdentity>(null);

            // validate the credentials
            if (await ValidCredentialsAsync(userToVerify, password))
            {
                return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(userName, userToVerify.Id));
            }

            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<ClaimsIdentity>(null);
        }
        async Task<AppUser> GetDummyUser(string userName, string password) {
          var user = new AppUser();
          user.FirstName = "Darkwing";
          user.LastName = "Duck";
          user.Username = userName;
          user.Password = password;

          return await Task.FromResult(user);
        }
        async Task<bool> ValidCredentialsAsync(string userName, string password) {
            var isValid = !string.IsNullOrWhiteSpace(userName) &&
                          !string.IsNullOrWhiteSpace(password);
          return await Task.FromResult(isValid);
        }
        async Task<bool> ValidCredentialsAsync(AppUser user, string password) {
            var isValid = user != null &&
                   !string.IsNullOrWhiteSpace(user.Password) &&
                   !string.IsNullOrWhiteSpace(password) &&
                   user.Password.Equals(password, StringComparison.InvariantCulture);
          return await Task.FromResult(isValid);
        }
    }
}
