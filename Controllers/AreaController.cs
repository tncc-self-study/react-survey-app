using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using react_survey_app.Data.Entities;
using react_survey_app.Data.Infrastructure;
using react_survey_app.Models;

namespace react_survey_app.Controllers
{
    [Authorize]
    [Route("api/areas")]
    public class AreaController : Controller
    {
        ILogger<AreaController> _logger;
        IRepository _repository;

        public AreaController(ILogger<AreaController> logger,
                              IRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var data = (await _repository.AllAsync<Area>()).OrderBy(x => x.Index).ToList();
            var count = data.Count();
            var result = data.Select(x => new DropdownItem { Id = x.Id, Label = x.Description, Value = x.Description});
            _logger.LogDebug($"Fetched {nameof(AreaController)} Areas ({count})");
            return Ok(result);
        }
    }
}
