using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using react_survey_app.Data.Entities;
using react_survey_app.Data.Infrastructure;
using react_survey_app.Models;

namespace react_survey_app.Controllers
{
    [Authorize]
    [Route("api/salesreps")]
    public class SalesRepController : Controller
    {
        ILogger<SalesRepController> _logger;
        IRepository _repository;

        public SalesRepController(ILogger<SalesRepController> logger,
                                  IRepository repository)
        {
            _logger = logger;
            _repository = repository;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var data = (await _repository.AllAsync<SalesRep>()).OrderBy(x => x.Index).ToList();
            var count = data.Count();
            var result = data.Select(x => new DropdownItem { Id = x.Id, Label = x.Description, Value = x.Description});
            _logger.LogDebug($"Fetched {nameof(AreaController)} SalesReps ({count})");
            return Ok(result);
        }
    }
}
