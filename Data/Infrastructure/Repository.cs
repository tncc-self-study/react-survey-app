namespace react_survey_app.Data.Infrastructure
{
    using MongoDB.Bson;
    using MongoDB.Driver;
    using MongoDbGenericRepository;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;

    public sealed class Repository : IRepository
    {
        #region Constants

        private const string Ident = "_id";

        #endregion Constants

        #region Fields

        private readonly IMongoDatabase _db;

        #endregion Fields

        #region Constructor

        public Repository(IMongoDbContext ctx)
        {
            _db = ctx.Database;
        }

        #endregion Constructor

        #region Methods

        public async Task<TEntity> SingleAsync<TEntity>(object key) where TEntity : class, new()
        {
            var collection = GetCollection<TEntity>();
            var query = Builders<TEntity>.Filter.Eq(Ident, new ObjectId(key.ToString()));
            var entity = await collection.Find(query).SingleAsync();

            if (entity == null) throw new NullReferenceException("Document with key '" + key + "' not found.");

            return entity;
        }

        public async Task<IEnumerable<TEntity>> AllAsync<TEntity>() where TEntity : class, new()
        {
            var collection = GetCollection<TEntity>();
            var query = Builders<TEntity>.Filter.Empty;
            return await collection.Find(query).ToListAsync();
        }

        public async Task<IMongoCollection<TEntity>> AllAsMongoCollectionAsync<TEntity>() where TEntity : class, new()
        {
            var collection = GetCollection<TEntity>();
            return await Task.FromResult(collection);
        }

        public async Task<bool> ExistsAsync<TEntity>(object key) where TEntity : class, new()
        {
            var collection = GetCollection<TEntity>();
            var query = Builders<TEntity>.Filter.Eq(Ident, new ObjectId(key.ToString()));
            var exists = await collection.Find(query).AnyAsync();
            return exists;
        }

        public async Task<IEnumerable<TEntity>> GetManyAsync<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class, new()
        {
            var collection = GetCollection<TEntity>();
            var query = filter.Compile();
            var result = collection.AsQueryable().Where(query).ToList();
            return await Task.FromResult(result);
        }

        public async Task InsertAsync<TEntity>(TEntity item) where TEntity : class, new()
        {
            var collection = GetCollection<TEntity>();
            await collection.InsertOneAsync(item);
        }

        public async Task UpdateAsync<TEntity>(TEntity item) where TEntity : class, new()
        {
            var collection = GetCollection<TEntity>();
            var key = GetKey(item);
            var query = Builders<TEntity>.Filter.Eq(Ident, new ObjectId(key.Id));
            var options = new FindOneAndReplaceOptions<TEntity> { IsUpsert = true };
            await collection.FindOneAndReplaceAsync(query, item, options);
        }

        public async Task DeleteAsync<TEntity>(object key) where TEntity : class, new()
        {
            var collection = GetCollection<TEntity>();
            var query = Builders<TEntity>.Filter.Eq(Ident, new ObjectId(key.ToString()));
            await collection.DeleteOneAsync(query);
        }

        private IMongoCollection<TEntity> GetCollection<TEntity>()
        {
            return _db.GetCollection<TEntity>(typeof(TEntity).Name.ToLower());
        }

        private static IEntity GetKey<TEntity>(TEntity item) where TEntity : class, new()
        {
            return item as IEntity;
        }

        #endregion Methods
    }
}