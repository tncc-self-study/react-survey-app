namespace react_survey_app.Data.Infrastructure
{
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Attributes;
    using System;
    public interface IChangeTracking
    {
        [BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime)]
        DateTime Created { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        string CreatedBy { get; set; }

        [BsonDateTimeOptions(Kind = DateTimeKind.Utc, Representation = BsonType.DateTime)]
        DateTime? Amended { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        string AmendedBy { get; set; }
    }
}