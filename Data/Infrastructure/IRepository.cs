using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace react_survey_app.Data.Infrastructure
{
    public interface IRepository
    {
        Task<TEntity> SingleAsync<TEntity>(object key) where TEntity : class, new();

        Task<IEnumerable<TEntity>> AllAsync<TEntity>() where TEntity : class, new();

        Task<IMongoCollection<TEntity>> AllAsMongoCollectionAsync<TEntity>() where TEntity : class, new();

        Task<bool> ExistsAsync<TEntity>(object key) where TEntity : class, new();

        Task<IEnumerable<TEntity>> GetManyAsync<TEntity>(Expression<Func<TEntity, bool>> filter) where TEntity : class, new();

        Task InsertAsync<TEntity>(TEntity item) where TEntity : class, new();

        Task UpdateAsync<TEntity>(TEntity item) where TEntity : class, new();

        Task DeleteAsync<TEntity>(object key) where TEntity : class, new();
    }
}