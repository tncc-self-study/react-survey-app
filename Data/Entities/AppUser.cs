using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using react_survey_app.Data.Infrastructure;

namespace react_survey_app.Data.Entities
{
    public class AppUser : IEntity
    {
      public AppUser()
      {
          Id = ObjectId.GenerateNewId().ToString();
      }

      [BsonRepresentation(BsonType.ObjectId)]
      public string Id { get ; set; }

      public string FirstName { get; set; }

      public string LastName { get; set; }

      public string Username { get; set; }

      public string Password { get; set; }
    }
}
