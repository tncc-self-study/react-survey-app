using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace react_survey_app.Data.Entities
{
    public class SalesRep
    {
        public SalesRep()
        {
            Id = ObjectId.GenerateNewId().ToString();
        }

        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get ; set; }

        [Required]
        [BsonElement("description")]
        public string Description { get; set; }

        [BsonElement("index")]
        public int Index { get;set; }
        
        #region Overrides

        public override string ToString()
        {
            return $"{Description}";
        }

        #endregion
    }
}