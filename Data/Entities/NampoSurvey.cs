namespace react_survey_app.Data.Entities
{
    using System;
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Attributes;
    using react_survey_app.Contracts;
    using react_survey_app.Data.Infrastructure;

    public class NampoSurvey : IEntity, IChangeTracking, INampoSurvey
    {
        public NampoSurvey()
        {
            Id = ObjectId.GenerateNewId().ToString();
            Created = DateTime.UtcNow;
            CreatedBy = "API User";
        }

        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public DateTime Created { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? Amended { get; set; }
        public string AmendedBy { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("mobile")]
        public string Mobile { get; set; }

        [BsonElement("fax")]
        public string Fax { get; set; }

        [BsonElement("telephone")]
        public string Telephone { get; set; }

        [BsonElement("town")]
        public string Town { get; set; }

        [BsonElement("area")]
        public string Area { get; set; }

        [BsonElement("iscurrentcustomer")]
        public bool IsCurrentCustomer { get; set; }

        [BsonElement("customername")]
        public string CustomerName { get; set; }

        [BsonElement("mercedes")]
        public bool Mercedes { get; set; }

        [BsonElement("freightliner")]
        public bool FreightLiner { get; set; }

        [BsonElement("iveco")]
        public bool Iveco { get; set; }

        [BsonElement("truckbodyparts")]
        public bool Truckbodyparts { get; set; }

        [BsonElement("man")]
        public bool Man { get; set; }

        [BsonElement("volvo")]
        public bool Volvo { get; set; }

        [BsonElement("international")]
        public bool International { get; set; }

        [BsonElement("tankers")]
        public bool Tankers { get; set; }

        [BsonElement("jonesco")]
        public bool Jonesco { get; set; }

        [BsonElement("truckaccessories")]
        public bool Truckaccessories { get; set; }

        [BsonElement("scania")]
        public bool Scania { get; set; }

        [BsonElement("renault")]
        public bool Renault { get; set; }

        [BsonElement("trailers")]
        public bool Trailers { get; set; }

        [BsonElement("fueldefend")]
        public bool Fueldefend { get; set; }

        [BsonElement("daf")]
        public bool Daf { get; set; }

        [BsonElement("otherproduct")]
        public bool OtherProduct { get; set; }

        [BsonElement("transporter")]
        public bool Transporter { get; set; }

        [BsonElement("distribution")]
        public bool Distributor { get; set; }

        [BsonElement("manufacturer")]
        public bool Manufacturer { get; set; }

        [BsonElement("reseller")]
        public bool Reseller { get; set; }

        [BsonElement("farmer")]
        public bool Farmer { get; set; }

        [BsonElement("earthmoving")]
        public bool Earthmoving { get; set; }

        [BsonElement("heavyhaulage")]
        public bool Heavyhaulage { get; set; }

        [BsonElement("crossborder")]
        public bool Crossborder { get; set; }

        [BsonElement("otherindustry")]
        public bool OtherIndustry { get; set; }

        [BsonElement("comments")]
        public string Comments { get; set; }

        [BsonElement("isurgent")]
        public bool IsUrgent { get; set; }

        [BsonElement("rep")]
        public string Rep { get; set; }
    }
}
