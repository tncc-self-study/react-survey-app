namespace react_survey_app.Models
{
    public class DropdownItem
    {
        public string Id { get; set; }
        public string Label { get;set; }
        public string Value { get;set; }
    }
}