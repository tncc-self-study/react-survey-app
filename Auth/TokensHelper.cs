using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Newtonsoft.Json;
using react_survey_app.Models;

namespace react_survey_app.Auth
{
    public static class TokensHelper
    {
      public static async Task<string> GenerateJwtAsync(ClaimsIdentity identity, IJwtFactory jwtFactory,string userName, JwtIssuerOptions jwtOptions, JsonSerializerSettings serializerSettings)
      {
        var response = new
        {
          id = identity.Claims.Single(c => c.Type == "id").Value,
          auth_token = await jwtFactory.GenerateEncodedTokenAsync(userName, identity),
          expires_in = (int)jwtOptions.ValidFor.TotalSeconds
        };

        return JsonConvert.SerializeObject(response, serializerSettings);
      }

      public static class Constants
      {
          public static class Strings
          {
              public static class JwtClaimIdentifiers
              {
                  public const string Rol = "rol", Id = "id";
              }

              public static class JwtClaims
              {
                  public const string ApiAccess = "api_access";
              }
          }
      }
    }
}
